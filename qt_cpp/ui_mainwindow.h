/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.3.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QPlainTextEdit *plainTextEdit;
    QPushButton *stopButton;
    QTabWidget *tabWidget;
    QWidget *tab_1;
    QLabel *label_7;
    QLabel *aircraftLabel;
    QLabel *label_8;
    QLabel *engineTypeLabel;
    QLabel *label_12;
    QLabel *transponderLabel;
    QLabel *label_24;
    QLabel *atcNameLabel;
    QWidget *tab_2;
    QLabel *compassLabel;
    QLabel *airspeedLabel;
    QLabel *label_2;
    QLabel *label;
    QLabel *groundSpeedLabel;
    QLabel *label_3;
    QLabel *verticalSpeedLabel;
    QLabel *label_9;
    QLabel *eng1levelLabel;
    QLabel *label_10;
    QLabel *altimeterLabel;
    QLabel *label_16;
    QLabel *label_17;
    QLabel *groundFlagLabel;
    QLabel *label_18;
    QLabel *latitudeLabel;
    QLabel *longitudeLabel;
    QLabel *label_19;
    QLabel *accelerationLabel;
    QLabel *label_20;
    QLabel *label_21;
    QLabel *altitudeLabel;
    QLabel *headingLabel;
    QLabel *label_25;
    QLabel *eng2levelLabel;
    QLabel *vor1ETALabel;
    QLabel *label_4;
    QLabel *vor1SpeedLabel;
    QLabel *vor1DistLabel;
    QLabel *label_6;
    QLabel *label_5;
    QLabel *label_11;
    QLabel *nav2FreqLabel;
    QLabel *label_15;
    QLabel *com1FreqLabel;
    QLabel *label_28;
    QLabel *label_31;
    QLabel *label_32;
    QLabel *com2FreqLabel;
    QLabel *nav1FreqLabel;
    QLabel *label_33;
    QWidget *tab_4;
    QPushButton *setAutopilotButton;
    QPushButton *autopilotOffButton;
    QPushButton *lightsButton;
    QSpinBox *autopilotHeadingBox;
    QLabel *label_29;
    QSpinBox *autopilotAltitudeBox;
    QLabel *autopilotLabel;
    QLabel *label_26;
    QComboBox *headNAVcomboBox;
    QCheckBox *fullThrottleCheckBox;
    QPushButton *airportButton;
    QLabel *label_30;
    QSpinBox *autopilotIASBox;
    QWidget *tab_5;
    QPlainTextEdit *aboutTextEdit;
    QPushButton *startButton;
    QLabel *label_27;
    QLabel *aircraftTitleLabel;
    QMenuBar *menuBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(584, 650);
        MainWindow->setMinimumSize(QSize(584, 417));
        MainWindow->setMaximumSize(QSize(584, 650));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        plainTextEdit = new QPlainTextEdit(centralWidget);
        plainTextEdit->setObjectName(QStringLiteral("plainTextEdit"));
        plainTextEdit->setEnabled(true);
        plainTextEdit->setGeometry(QRect(10, 550, 561, 61));
        plainTextEdit->setUndoRedoEnabled(false);
        plainTextEdit->setReadOnly(true);
        stopButton = new QPushButton(centralWidget);
        stopButton->setObjectName(QStringLiteral("stopButton"));
        stopButton->setGeometry(QRect(500, 10, 75, 23));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabWidget->setGeometry(QRect(10, 60, 561, 481));
        tab_1 = new QWidget();
        tab_1->setObjectName(QStringLiteral("tab_1"));
        label_7 = new QLabel(tab_1);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(20, 20, 101, 16));
        aircraftLabel = new QLabel(tab_1);
        aircraftLabel->setObjectName(QStringLiteral("aircraftLabel"));
        aircraftLabel->setGeometry(QRect(130, 20, 111, 16));
        label_8 = new QLabel(tab_1);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(20, 50, 101, 16));
        engineTypeLabel = new QLabel(tab_1);
        engineTypeLabel->setObjectName(QStringLiteral("engineTypeLabel"));
        engineTypeLabel->setGeometry(QRect(130, 50, 111, 16));
        label_12 = new QLabel(tab_1);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setGeometry(QRect(20, 80, 101, 16));
        transponderLabel = new QLabel(tab_1);
        transponderLabel->setObjectName(QStringLiteral("transponderLabel"));
        transponderLabel->setGeometry(QRect(130, 80, 111, 16));
        label_24 = new QLabel(tab_1);
        label_24->setObjectName(QStringLiteral("label_24"));
        label_24->setGeometry(QRect(20, 110, 101, 16));
        atcNameLabel = new QLabel(tab_1);
        atcNameLabel->setObjectName(QStringLiteral("atcNameLabel"));
        atcNameLabel->setGeometry(QRect(130, 110, 111, 16));
        tabWidget->addTab(tab_1, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QStringLiteral("tab_2"));
        compassLabel = new QLabel(tab_2);
        compassLabel->setObjectName(QStringLiteral("compassLabel"));
        compassLabel->setGeometry(QRect(140, 80, 111, 16));
        airspeedLabel = new QLabel(tab_2);
        airspeedLabel->setObjectName(QStringLiteral("airspeedLabel"));
        airspeedLabel->setGeometry(QRect(140, 50, 111, 16));
        label_2 = new QLabel(tab_2);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(20, 50, 111, 16));
        label = new QLabel(tab_2);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(20, 20, 111, 16));
        groundSpeedLabel = new QLabel(tab_2);
        groundSpeedLabel->setObjectName(QStringLiteral("groundSpeedLabel"));
        groundSpeedLabel->setGeometry(QRect(140, 20, 111, 16));
        label_3 = new QLabel(tab_2);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(20, 80, 111, 16));
        verticalSpeedLabel = new QLabel(tab_2);
        verticalSpeedLabel->setObjectName(QStringLiteral("verticalSpeedLabel"));
        verticalSpeedLabel->setGeometry(QRect(410, 80, 111, 16));
        label_9 = new QLabel(tab_2);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(290, 80, 111, 16));
        eng1levelLabel = new QLabel(tab_2);
        eng1levelLabel->setObjectName(QStringLiteral("eng1levelLabel"));
        eng1levelLabel->setGeometry(QRect(410, 170, 51, 16));
        label_10 = new QLabel(tab_2);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setGeometry(QRect(290, 170, 111, 16));
        altimeterLabel = new QLabel(tab_2);
        altimeterLabel->setObjectName(QStringLiteral("altimeterLabel"));
        altimeterLabel->setGeometry(QRect(140, 110, 111, 16));
        label_16 = new QLabel(tab_2);
        label_16->setObjectName(QStringLiteral("label_16"));
        label_16->setGeometry(QRect(20, 110, 111, 16));
        label_17 = new QLabel(tab_2);
        label_17->setObjectName(QStringLiteral("label_17"));
        label_17->setGeometry(QRect(290, 20, 111, 16));
        groundFlagLabel = new QLabel(tab_2);
        groundFlagLabel->setObjectName(QStringLiteral("groundFlagLabel"));
        groundFlagLabel->setGeometry(QRect(410, 20, 111, 16));
        label_18 = new QLabel(tab_2);
        label_18->setObjectName(QStringLiteral("label_18"));
        label_18->setGeometry(QRect(290, 110, 111, 16));
        latitudeLabel = new QLabel(tab_2);
        latitudeLabel->setObjectName(QStringLiteral("latitudeLabel"));
        latitudeLabel->setGeometry(QRect(410, 110, 111, 16));
        longitudeLabel = new QLabel(tab_2);
        longitudeLabel->setObjectName(QStringLiteral("longitudeLabel"));
        longitudeLabel->setGeometry(QRect(410, 140, 111, 16));
        label_19 = new QLabel(tab_2);
        label_19->setObjectName(QStringLiteral("label_19"));
        label_19->setGeometry(QRect(290, 140, 111, 16));
        accelerationLabel = new QLabel(tab_2);
        accelerationLabel->setObjectName(QStringLiteral("accelerationLabel"));
        accelerationLabel->setGeometry(QRect(410, 50, 111, 16));
        label_20 = new QLabel(tab_2);
        label_20->setObjectName(QStringLiteral("label_20"));
        label_20->setGeometry(QRect(290, 50, 111, 16));
        label_21 = new QLabel(tab_2);
        label_21->setObjectName(QStringLiteral("label_21"));
        label_21->setGeometry(QRect(20, 140, 111, 16));
        altitudeLabel = new QLabel(tab_2);
        altitudeLabel->setObjectName(QStringLiteral("altitudeLabel"));
        altitudeLabel->setGeometry(QRect(140, 140, 111, 16));
        headingLabel = new QLabel(tab_2);
        headingLabel->setObjectName(QStringLiteral("headingLabel"));
        headingLabel->setGeometry(QRect(140, 170, 111, 16));
        label_25 = new QLabel(tab_2);
        label_25->setObjectName(QStringLiteral("label_25"));
        label_25->setGeometry(QRect(20, 170, 111, 16));
        eng2levelLabel = new QLabel(tab_2);
        eng2levelLabel->setObjectName(QStringLiteral("eng2levelLabel"));
        eng2levelLabel->setGeometry(QRect(460, 170, 51, 16));
        vor1ETALabel = new QLabel(tab_2);
        vor1ETALabel->setObjectName(QStringLiteral("vor1ETALabel"));
        vor1ETALabel->setGeometry(QRect(140, 320, 111, 16));
        label_4 = new QLabel(tab_2);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(20, 260, 121, 16));
        vor1SpeedLabel = new QLabel(tab_2);
        vor1SpeedLabel->setObjectName(QStringLiteral("vor1SpeedLabel"));
        vor1SpeedLabel->setGeometry(QRect(140, 290, 111, 16));
        vor1DistLabel = new QLabel(tab_2);
        vor1DistLabel->setObjectName(QStringLiteral("vor1DistLabel"));
        vor1DistLabel->setGeometry(QRect(140, 260, 111, 16));
        label_6 = new QLabel(tab_2);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(20, 320, 121, 16));
        label_5 = new QLabel(tab_2);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(20, 290, 121, 16));
        label_11 = new QLabel(tab_2);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setGeometry(QRect(20, 230, 46, 13));
        QFont font;
        font.setPointSize(9);
        font.setBold(true);
        font.setWeight(75);
        label_11->setFont(font);
        nav2FreqLabel = new QLabel(tab_2);
        nav2FreqLabel->setObjectName(QStringLiteral("nav2FreqLabel"));
        nav2FreqLabel->setGeometry(QRect(140, 410, 111, 16));
        label_15 = new QLabel(tab_2);
        label_15->setObjectName(QStringLiteral("label_15"));
        label_15->setGeometry(QRect(290, 290, 121, 16));
        com1FreqLabel = new QLabel(tab_2);
        com1FreqLabel->setObjectName(QStringLiteral("com1FreqLabel"));
        com1FreqLabel->setGeometry(QRect(410, 260, 111, 16));
        label_28 = new QLabel(tab_2);
        label_28->setObjectName(QStringLiteral("label_28"));
        label_28->setGeometry(QRect(20, 410, 121, 16));
        label_31 = new QLabel(tab_2);
        label_31->setObjectName(QStringLiteral("label_31"));
        label_31->setGeometry(QRect(20, 380, 121, 16));
        label_32 = new QLabel(tab_2);
        label_32->setObjectName(QStringLiteral("label_32"));
        label_32->setGeometry(QRect(290, 260, 121, 16));
        com2FreqLabel = new QLabel(tab_2);
        com2FreqLabel->setObjectName(QStringLiteral("com2FreqLabel"));
        com2FreqLabel->setGeometry(QRect(410, 290, 111, 16));
        nav1FreqLabel = new QLabel(tab_2);
        nav1FreqLabel->setObjectName(QStringLiteral("nav1FreqLabel"));
        nav1FreqLabel->setGeometry(QRect(140, 380, 111, 16));
        label_33 = new QLabel(tab_2);
        label_33->setObjectName(QStringLiteral("label_33"));
        label_33->setGeometry(QRect(290, 230, 46, 13));
        label_33->setFont(font);
        tabWidget->addTab(tab_2, QString());
        tab_4 = new QWidget();
        tab_4->setObjectName(QStringLiteral("tab_4"));
        setAutopilotButton = new QPushButton(tab_4);
        setAutopilotButton->setObjectName(QStringLiteral("setAutopilotButton"));
        setAutopilotButton->setGeometry(QRect(30, 280, 91, 23));
        setAutopilotButton->setContextMenuPolicy(Qt::NoContextMenu);
        autopilotOffButton = new QPushButton(tab_4);
        autopilotOffButton->setObjectName(QStringLiteral("autopilotOffButton"));
        autopilotOffButton->setGeometry(QRect(460, 200, 75, 23));
        lightsButton = new QPushButton(tab_4);
        lightsButton->setObjectName(QStringLiteral("lightsButton"));
        lightsButton->setGeometry(QRect(460, 20, 75, 23));
        autopilotHeadingBox = new QSpinBox(tab_4);
        autopilotHeadingBox->setObjectName(QStringLiteral("autopilotHeadingBox"));
        autopilotHeadingBox->setGeometry(QRect(100, 70, 71, 20));
        autopilotHeadingBox->setMinimum(-3);
        autopilotHeadingBox->setMaximum(359);
        autopilotHeadingBox->setValue(356);
        label_29 = new QLabel(tab_4);
        label_29->setObjectName(QStringLiteral("label_29"));
        label_29->setGeometry(QRect(20, 110, 71, 16));
        autopilotAltitudeBox = new QSpinBox(tab_4);
        autopilotAltitudeBox->setObjectName(QStringLiteral("autopilotAltitudeBox"));
        autopilotAltitudeBox->setGeometry(QRect(100, 110, 71, 20));
        autopilotAltitudeBox->setMinimum(500);
        autopilotAltitudeBox->setMaximum(4000);
        autopilotAltitudeBox->setValue(1500);
        autopilotLabel = new QLabel(tab_4);
        autopilotLabel->setObjectName(QStringLiteral("autopilotLabel"));
        autopilotLabel->setGeometry(QRect(90, 20, 111, 16));
        label_26 = new QLabel(tab_4);
        label_26->setObjectName(QStringLiteral("label_26"));
        label_26->setGeometry(QRect(20, 20, 71, 16));
        headNAVcomboBox = new QComboBox(tab_4);
        headNAVcomboBox->setObjectName(QStringLiteral("headNAVcomboBox"));
        headNAVcomboBox->setGeometry(QRect(20, 70, 71, 22));
        fullThrottleCheckBox = new QCheckBox(tab_4);
        fullThrottleCheckBox->setObjectName(QStringLiteral("fullThrottleCheckBox"));
        fullThrottleCheckBox->setGeometry(QRect(20, 150, 141, 17));
        fullThrottleCheckBox->setChecked(true);
        airportButton = new QPushButton(tab_4);
        airportButton->setObjectName(QStringLiteral("airportButton"));
        airportButton->setGeometry(QRect(460, 70, 75, 23));
        label_30 = new QLabel(tab_4);
        label_30->setObjectName(QStringLiteral("label_30"));
        label_30->setGeometry(QRect(20, 210, 71, 16));
        autopilotIASBox = new QSpinBox(tab_4);
        autopilotIASBox->setObjectName(QStringLiteral("autopilotIASBox"));
        autopilotIASBox->setGeometry(QRect(100, 210, 71, 20));
        autopilotIASBox->setMinimum(160);
        autopilotIASBox->setMaximum(300);
        autopilotIASBox->setValue(200);
        tabWidget->addTab(tab_4, QString());
        tab_5 = new QWidget();
        tab_5->setObjectName(QStringLiteral("tab_5"));
        aboutTextEdit = new QPlainTextEdit(tab_5);
        aboutTextEdit->setObjectName(QStringLiteral("aboutTextEdit"));
        aboutTextEdit->setGeometry(QRect(20, 20, 521, 421));
        aboutTextEdit->setReadOnly(true);
        tabWidget->addTab(tab_5, QString());
        startButton = new QPushButton(centralWidget);
        startButton->setObjectName(QStringLiteral("startButton"));
        startButton->setGeometry(QRect(410, 10, 75, 23));
        label_27 = new QLabel(centralWidget);
        label_27->setObjectName(QStringLiteral("label_27"));
        label_27->setGeometry(QRect(20, 10, 241, 16));
        QFont font1;
        font1.setFamily(QStringLiteral("Arial"));
        font1.setPointSize(12);
        font1.setBold(true);
        font1.setWeight(75);
        label_27->setFont(font1);
        aircraftTitleLabel = new QLabel(centralWidget);
        aircraftTitleLabel->setObjectName(QStringLiteral("aircraftTitleLabel"));
        aircraftTitleLabel->setGeometry(QRect(20, 30, 280, 16));
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 584, 21));
        MainWindow->setMenuBar(menuBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        stopButton->setText(QApplication::translate("MainWindow", "Stop", 0));
        label_7->setText(QApplication::translate("MainWindow", "Aircraft:", 0));
        aircraftLabel->setText(QApplication::translate("MainWindow", "-", 0));
        label_8->setText(QApplication::translate("MainWindow", "Engine Type:", 0));
        engineTypeLabel->setText(QApplication::translate("MainWindow", "-", 0));
        label_12->setText(QApplication::translate("MainWindow", "Transponder:", 0));
        transponderLabel->setText(QApplication::translate("MainWindow", "-", 0));
        label_24->setText(QApplication::translate("MainWindow", "ATC Name:", 0));
        atcNameLabel->setText(QApplication::translate("MainWindow", "-", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_1), QApplication::translate("MainWindow", "Aircraft ID", 0));
        compassLabel->setText(QApplication::translate("MainWindow", "-", 0));
        airspeedLabel->setText(QApplication::translate("MainWindow", "-", 0));
        label_2->setText(QApplication::translate("MainWindow", "TAS Airspeed (kts):", 0));
        label->setText(QApplication::translate("MainWindow", "Ground Speed (m/s):", 0));
        groundSpeedLabel->setText(QApplication::translate("MainWindow", "-", 0));
        label_3->setText(QApplication::translate("MainWindow", "Compass:", 0));
        verticalSpeedLabel->setText(QApplication::translate("MainWindow", "-", 0));
        label_9->setText(QApplication::translate("MainWindow", "Vertical Speed (ft/min):", 0));
        eng1levelLabel->setText(QApplication::translate("MainWindow", "-", 0));
        label_10->setText(QApplication::translate("MainWindow", "Throttle Eng 1-2 (%):", 0));
        altimeterLabel->setText(QApplication::translate("MainWindow", "-", 0));
        label_16->setText(QApplication::translate("MainWindow", "Altimeter:", 0));
        label_17->setText(QApplication::translate("MainWindow", "Ground Flag:", 0));
        groundFlagLabel->setText(QApplication::translate("MainWindow", "-", 0));
        label_18->setText(QApplication::translate("MainWindow", "Latitude:", 0));
        latitudeLabel->setText(QApplication::translate("MainWindow", "-", 0));
        longitudeLabel->setText(QApplication::translate("MainWindow", "-", 0));
        label_19->setText(QApplication::translate("MainWindow", "Longitude:", 0));
        accelerationLabel->setText(QApplication::translate("MainWindow", "-", 0));
        label_20->setText(QApplication::translate("MainWindow", "Acceleration (g):", 0));
        label_21->setText(QApplication::translate("MainWindow", "Altitude:", 0));
        altitudeLabel->setText(QApplication::translate("MainWindow", "-", 0));
        headingLabel->setText(QApplication::translate("MainWindow", "-", 0));
        label_25->setText(QApplication::translate("MainWindow", "Heading Marker:", 0));
        eng2levelLabel->setText(QApplication::translate("MainWindow", "-", 0));
        vor1ETALabel->setText(QApplication::translate("MainWindow", "-", 0));
        label_4->setText(QApplication::translate("MainWindow", "VOR1 DME Dist (nm):", 0));
        vor1SpeedLabel->setText(QApplication::translate("MainWindow", "-", 0));
        vor1DistLabel->setText(QApplication::translate("MainWindow", "-", 0));
        label_6->setText(QApplication::translate("MainWindow", "VOR1 DME ETA (secs):", 0));
        label_5->setText(QApplication::translate("MainWindow", "VOR1 DME Speed (kts):", 0));
        label_11->setText(QApplication::translate("MainWindow", "VOR", 0));
        nav2FreqLabel->setText(QApplication::translate("MainWindow", "-", 0));
        label_15->setText(QApplication::translate("MainWindow", "COM2 Freq (MHz):", 0));
        com1FreqLabel->setText(QApplication::translate("MainWindow", "-", 0));
        label_28->setText(QApplication::translate("MainWindow", "NAV2 Freq (MHz):", 0));
        label_31->setText(QApplication::translate("MainWindow", "NAV1 Freq (MHz):", 0));
        label_32->setText(QApplication::translate("MainWindow", "COM1 Freq (MHz):", 0));
        com2FreqLabel->setText(QApplication::translate("MainWindow", "-", 0));
        nav1FreqLabel->setText(QApplication::translate("MainWindow", "-", 0));
        label_33->setText(QApplication::translate("MainWindow", "COM", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("MainWindow", "Aircraft Conditions", 0));
        setAutopilotButton->setText(QApplication::translate("MainWindow", "Set Autopilot", 0));
        autopilotOffButton->setText(QApplication::translate("MainWindow", "Autopilot Off", 0));
        lightsButton->setText(QApplication::translate("MainWindow", "Lights", 0));
        label_29->setText(QApplication::translate("MainWindow", "Altitude:", 0));
        autopilotLabel->setText(QApplication::translate("MainWindow", "-", 0));
        label_26->setText(QApplication::translate("MainWindow", "Autopilot:", 0));
        headNAVcomboBox->clear();
        headNAVcomboBox->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "Heading", 0)
         << QApplication::translate("MainWindow", "NAV1", 0)
        );
        fullThrottleCheckBox->setText(QApplication::translate("MainWindow", "Full Throttle", 0));
        airportButton->setText(QApplication::translate("MainWindow", "Airport LPPR", 0));
        label_30->setText(QApplication::translate("MainWindow", "IAS w A/T:", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_4), QApplication::translate("MainWindow", "Control", 0));
        aboutTextEdit->setPlainText(QString());
        tabWidget->setTabText(tabWidget->indexOf(tab_5), QApplication::translate("MainWindow", "About", 0));
        startButton->setText(QApplication::translate("MainWindow", "Start", 0));
        label_27->setText(QApplication::translate("MainWindow", "FS2004 Reader v1.0", 0));
        aircraftTitleLabel->setText(QApplication::translate("MainWindow", "-", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
