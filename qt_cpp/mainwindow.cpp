/*
 * libc.lib - http://support.microsoft.com/kb/154419/pt-br
 * http://www.projectmagenta.com/all-fsuipc-offsets/
 * http://forum.simflight.com/topic/70770-reading-out-some-values-from-fs2004-qt/
 * http://forums.vatsim.net/viewtopic.php?t=42673
 * http://forums.overclockers.co.uk/showthread.php?p=14524309
 * http://forum.simflight.com/topic/68195-vrinsight-mcp-combo-and-fsuipc-lua-scripting/
 * https://github.com/apoloval/open-airbus-cockpit/blob/master/oacsd/wilco-exporter/src/mapping.cpp
 *
 * System Americano - US System
 */

#define NOMINMAX // avoid qtime problem
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtDebug>
#include <QTime>
#include <QTimer>
#include <QPlainTextEdit>

#include <Windows.h> // #include <qt_windows.h>
#include "FSUIPC_User.h"
#include <stdlib.h>
#include <string.h>

static char chOurKey[] = "IKB3BI67TCHE"; // As obtained from Pete Dowson

DWORD dwResult;

struct airplane_id
{
    char title[256];
    char aircraft[24];
    char atc_name[12];
    short engine_type;
    char * engine_type_desc;
    short transponder;
    short landing_gear_type;
};

struct airplane_flight
{
    int ground_speed;
    int airspeed;
    short acceleration;
    double compass;
    long altimeter;
    short vertical_speed;
    double latitude;
    double longitude;
    double altitude;
    int heading;
    short eng1_level;
    short eng2_level;
};

struct airplane_nav
{
    short vor1_dist;
    short vor1_speed;
    short vor1_eta;
    short com1_freq;
    short com2_freq;
    short nav1_freq;
    short nav2_freq;
};

struct airplane_alert
{
    short crash_flag;
    short stall_flag;
    short overspeed_flag;
    short ground_flag;
    char * ground_flag_desc;
    short autopilot_flag;
    char * autopilot_flag_desc;
    short landing_gear_flag;
};

struct airplane_id id;
struct airplane_flight flight;
struct airplane_nav nav;
struct airplane_alert alert;


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    timerFlight(new QTimer),
    timerAircraft(new QTimer)
{
    ui->setupUi(this);

    // load about
    aboutDisplay();

    // start connection
    startFSUIPC();

    // set timer (2s)
    timerFlight->setInterval(2000);
    connect(timerFlight, SIGNAL(timeout()), this, SLOT(readFSUIPCFlight()));

    // set timer (15s)
    timerAircraft->setInterval(15000);
    connect(timerAircraft, SIGNAL(timeout()), this, SLOT(readFSUIPCAircraft()));
    readFSUIPCAircraft();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::aboutDisplay()
{
    ui->aboutTextEdit->insertPlainText(":: FS2004 Reader v1.0 ::\n\n"
                                       "ISEP - Instituto Superior de Engenharia do Porto\n"
                                       "SISTE - Sistemas de Telecomunicações na Aeronáutica, 2014\n\n"
                                       "Docente - Eng. José Carlos Barros Oliveira\n\n"
                                       "Aluno - Paulo Sérgio Guedes Portela (1081552)\n\n\n"
                                       ":: Software ::\n"
                                       "- MSFS2004 - Microsoft Flight Simulator 2004\n"
                                       "- FSUIPC - Flight Simulator Universal Inter Process Communication (http://www.schiratti.com/)");
}

void MainWindow::logDisplay(QString message)
{
    QString currentTime = QTime::currentTime().toString("hh:mm:ss");
    ui->plainTextEdit->insertPlainText("[" + currentTime + "] " + message + "\n");
    ui->plainTextEdit->ensureCursorVisible();
}

// bcd conversions
QString MainWindow::radioBCD2Str(short dec)
{
    short bcdH = 1 * 10 + (dec >> 12);
    bcdH = bcdH * 10 + (dec >> 8 & 0x000F);
    short bcdL = + (dec >> 4 & 0x000F);
    bcdL = bcdL * 10 + (dec & 0x000F);

    return QString::number(bcdH) + "." + QString::number(bcdL);
}

QString MainWindow::BCD2Str(short dec)
{
    short bcd = (dec >> 12);
    bcd = bcd * 10 + (dec >> 8 & 0x000F);
    bcd = bcd * 10 + (dec >> 4 & 0x000F);
    bcd = bcd * 10 + (dec & 0x000F);

    return QString::number(bcd);
}

// interface
void MainWindow::uiDisplay()
{
    // tab 1
    ui->aircraftTitleLabel->setText(id.title);
    ui->aircraftLabel->setText(id.aircraft);
    ui->engineTypeLabel->setText(id.engine_type_desc);
    ui->transponderLabel->setText(BCD2Str(id.transponder));
    ui->atcNameLabel->setText(id.atc_name);

    // tab 2
    ui->groundSpeedLabel->setText(QString::number(flight.ground_speed / 65536.0001));
    ui->airspeedLabel->setText(QString::number(flight.airspeed / 128.0));
    ui->accelerationLabel->setText(QString::number(flight.acceleration / 608.00001));
    ui->compassLabel->setText(QString::number(flight.compass));

    ui->altimeterLabel->setText(QString::number(flight.altimeter));
    ui->latitudeLabel->setText(QString::number(flight.latitude));
    ui->longitudeLabel->setText(QString::number(flight.longitude));
    ui->altitudeLabel->setText(QString::number(flight.altitude));
    ui->headingLabel->setText(QString::number(flight.heading * (360.0 / 65536.0)));
    ui->verticalSpeedLabel->setText(QString::number(flight.vertical_speed * 0.768946875));
    ui->eng1levelLabel->setText(QString::number(flight.eng1_level / 16384.0 * 100));
    ui->eng2levelLabel->setText(QString::number(flight.eng2_level / 16384.0 * 100));

    ui->groundFlagLabel->setText(alert.ground_flag_desc);

    // tab 3
    ui->vor1DistLabel->setText(QString::number(nav.vor1_dist / 10.0));
    ui->vor1SpeedLabel->setText(QString::number(nav.vor1_speed / 10.0));
    ui->vor1ETALabel->setText(QString::number(nav.vor1_eta / 10.00001));

    // tab 4
    ui->com1FreqLabel->setText(radioBCD2Str(nav.com1_freq));
    ui->com2FreqLabel->setText(radioBCD2Str(nav.com2_freq));
    ui->nav1FreqLabel->setText(radioBCD2Str(nav.nav1_freq));
    ui->nav2FreqLabel->setText(radioBCD2Str(nav.nav2_freq));

    // tab 5
    ui->autopilotLabel->setText(alert.autopilot_flag_desc);
}

void MainWindow::throttleControl(int level)
{
    int value = 0;
    if (level >= 0) value = level * 163.84; else value = -4096;
    FSUIPC_Write(0x089A, sizeof(value), &value, &dwResult); // eng. 1
    FSUIPC_Write(0x0932, sizeof(value), &value, &dwResult); // eng. 2
    FSUIPC_Write(0x09CA, sizeof(value), &value, &dwResult); // eng. 3
    FSUIPC_Write(0x0A62, sizeof(value), &value, &dwResult); // eng. 4
}

void MainWindow::control()
{
    if (alert.crash_flag == 1) logDisplay("crashed...");

    // stall - NEED PID IMPLEMENTATION FOR THIS
    if (alert.stall_flag == 1)
    {
        logDisplay("stall! applying full throttle");
        throttleControl(100);
    }

    // overspeed - NEED PID IMPLEMENTATION FOR THIS
    if (alert.overspeed_flag == 1)
    {
        logDisplay("overspeed... reducing throttle to 15%");
        throttleControl(15);
    }

    // landing gear
    if (id.landing_gear_type == 1)
    {
        // altimeter or altitude?
        if ((alert.landing_gear_flag == 0) && (flight.altitude < 500))
        {
            logDisplay("landing gear is up... getting down!");
            int aux = 16383;
            FSUIPC_Write(0x0BE8, 4, &aux, &dwResult);
            FSUIPC_Process(&dwResult);
        }

        if ((alert.landing_gear_flag == 16383) && (flight.altitude >= 500))
        {
            logDisplay("landing gear is down... getting up!");
            int aux = 0;
            FSUIPC_Write(0x0BE8, 4, &aux, &dwResult);
            FSUIPC_Process(&dwResult);
        }
    }
}

// fsuipc
void MainWindow::readFSUIPCAircraft()
{
    // type
    FSUIPC_Read(0x3D00, sizeof(id.title), &id.title, &dwResult);
    FSUIPC_Read(0x3160, sizeof(id.aircraft), &id.aircraft, &dwResult);
    FSUIPC_Read(0x0609, sizeof(id.engine_type), &id.engine_type, &dwResult);
    FSUIPC_Read(0x0354, sizeof(id.transponder), &id.transponder, &dwResult);
    FSUIPC_Read(0x313C, sizeof(id.atc_name), &id.atc_name, &dwResult);

    // NAO FUNCIONA NESTA VERSAO DO FS
    FSUIPC_Read(0x060E, sizeof(id.landing_gear_type), &id.landing_gear_type, &dwResult);

    switch (id.engine_type)
    {
    case 0:
        id.engine_type_desc = "Piston";
        break;
    case 1:
        id.engine_type_desc = "Jet";
        break;
    case 2:
        id.engine_type_desc = "Sailplane";
        break;
    case 3:
        id.engine_type_desc = "Helo (Bell) turbine";
        break;
    case 4:
        id.engine_type_desc = "Rocket";
        break;
    case 5:
        id.engine_type_desc = "Turboprop";
        break;
    default:
        id.engine_type_desc = "unknown";
    }

    if (FSUIPC_Process(&dwResult)) uiDisplay();
}

void MainWindow::readFSUIPCFlight()
{
    // crash and stall
    FSUIPC_Read(0x0840, sizeof(alert.crash_flag), &alert.crash_flag, &dwResult);
    FSUIPC_Read(0x036C, sizeof(alert.stall_flag), &alert.stall_flag, &dwResult);
    FSUIPC_Read(0x036D, sizeof(alert.overspeed_flag), &alert.overspeed_flag, &dwResult);
    FSUIPC_Read(0x0366, sizeof(alert.ground_flag), &alert.ground_flag, &dwResult);
    alert.ground_flag_desc = alert.ground_flag == 0 ? "airborne" : "on ground";
    FSUIPC_Read(0x0BE8, sizeof(alert.landing_gear_flag), &alert.landing_gear_flag, &dwResult);

    // autopilot
    FSUIPC_Read(0x07BC, sizeof(alert.autopilot_flag), &alert.autopilot_flag, &dwResult);
    alert.autopilot_flag_desc = alert.autopilot_flag == 1 ? "ON" : "OFF";

    // aircraft performance data
    FSUIPC_Read(0x02B4, sizeof(flight.ground_speed), &flight.ground_speed, &dwResult);
    FSUIPC_Read(0x02B8, sizeof(flight.airspeed), &flight.airspeed, &dwResult);
    FSUIPC_Read(0x11BA, sizeof(flight.acceleration), &flight.acceleration, &dwResult);
    FSUIPC_Read(0x2B00, sizeof(flight.compass), &flight.compass, &dwResult);
    FSUIPC_Read(0x3324, sizeof(flight.altimeter), &flight.altimeter, &dwResult);
    FSUIPC_Read(0x6010, sizeof(flight.latitude), &flight.latitude, &dwResult);
    FSUIPC_Read(0x6018, sizeof(flight.longitude), &flight.longitude, &dwResult);
    FSUIPC_Read(0x6020, sizeof(flight.altitude), &flight.altitude, &dwResult);
    FSUIPC_Read(0x07CC, sizeof(flight.heading), &flight.heading, &dwResult);
    FSUIPC_Read(0x02C8, sizeof(flight.vertical_speed), &flight.vertical_speed, &dwResult);

    FSUIPC_Read(0x089A, sizeof(flight.eng1_level), &flight.eng1_level, &dwResult);
    FSUIPC_Read(0x0932, sizeof(flight.eng2_level), &flight.eng2_level, &dwResult); // 0x092E

    // vor
    FSUIPC_Read(0x0300, sizeof(nav.vor1_dist), &nav.vor1_dist, &dwResult);
    FSUIPC_Read(0x0302, sizeof(nav.vor1_speed), &nav.vor1_speed, &dwResult);
    FSUIPC_Read(0x0304, sizeof(nav.vor1_eta), &nav.vor1_eta, &dwResult);

    // com
    FSUIPC_Read(0x034E, sizeof(nav.com1_freq), &nav.com1_freq, &dwResult);
    FSUIPC_Read(0x3118, sizeof(nav.com2_freq), &nav.com2_freq, &dwResult);
    FSUIPC_Read(0x350, sizeof(nav.nav1_freq), &nav.nav1_freq, &dwResult);
    FSUIPC_Read(0x352, sizeof(nav.nav2_freq), &nav.nav2_freq, &dwResult);

    // test zone
    short autopilotHeading;
    FSUIPC_Read(0x034E, sizeof(autopilotHeading), &autopilotHeading, &dwResult);

    if (FSUIPC_Process(&dwResult))
    {
        //qDebug() << autopilotHeading;
        control();
        uiDisplay();
    }
    else
        logDisplay("fsuipc process failure");
}


// FSUIPC (start / stop)
void MainWindow::startFSUIPC()
{
    if (FSUIPC_Open(SIM_FS2K4, &dwResult))
    {
        timerFlight->start();
        timerAircraft->start();
        logDisplay("starting engines");
    }
    else
    {
        logDisplay("could not start engines");
    }
}

void MainWindow::stopFSUIPC()
{
    FSUIPC_Close();
    timerFlight->stop();
    timerAircraft->stop();
    logDisplay("engine stop");
}

// butons
void MainWindow::on_startButton_clicked()
{
    startFSUIPC();
}

void MainWindow::on_stopButton_clicked()
{
    stopFSUIPC();
}

void MainWindow::on_lightsButton_clicked()
{
    //writeFSUIPC(0x0D0C, "3", sizeof(short));
    int aux = 3;
    FSUIPC_Write(0x0D0C, sizeof(aux), &aux, &dwResult);
    FSUIPC_Process(&dwResult);
}

void MainWindow::on_setAutopilotButton_clicked()
{
    // autopilot
    short autopilot = 1;
    FSUIPC_Write(0x07BC, sizeof(autopilot), &autopilot, &dwResult);

    // ias (boeing)
    short ias = ui->autopilotIASBox->value();
    FSUIPC_Write(0x07E2, sizeof(ias), &ias, &dwResult);
    short autoThrottleLock = 1;
    FSUIPC_Write(0x0810, sizeof(autoThrottleLock), &autoThrottleLock, &dwResult);
    short iasLock = 1;
    FSUIPC_Write(0x07DC, sizeof(iasLock), &iasLock, &dwResult);

    // altitude
    int uiAltitude = ui->autopilotAltitudeBox->value();
    long autopilotAltitude = uiAltitude * 19975;
    FSUIPC_Write(0x07D4, sizeof(autopilotAltitude), &autopilotAltitude, &dwResult);
    short autopilotAltitudeLock = 1;
    FSUIPC_Write(0x07D0, sizeof(autopilotAltitudeLock), &autopilotAltitudeLock, &dwResult);

    // vertical speed
    short autopilotVSpeed = 100;
    FSUIPC_Write(0x07F2, sizeof(autopilotVSpeed), &autopilotVSpeed, &dwResult);

    // heading
    int uiHeading = ui->autopilotHeadingBox->value();
    int autopilotHeading = uiHeading * (65536.0001 / 360.0);
    FSUIPC_Write(0x07CC, sizeof(autopilotHeading), &autopilotHeading, &dwResult);

    // full throttle
    short throttle = ui->fullThrottleCheckBox->checkState();
    if (throttle == 2) throttleControl(100); // tristate

    // choice
    short headNAVChoice = ui->headNAVcomboBox->currentIndex();

    short autopilotHeadingLock = 1;
    short autopilotNAV1Lock = 1;

    switch (headNAVChoice)
    {
    case 0:
        autopilotHeadingLock = 1;
        FSUIPC_Write(0x07C8, sizeof(autopilotHeadingLock), &autopilotHeadingLock, &dwResult);
        autopilotNAV1Lock = 0;
        FSUIPC_Write(0x07C4, sizeof(autopilotNAV1Lock), &autopilotNAV1Lock, &dwResult);
        break;
    case 1:
        autopilotHeadingLock = 0;
        FSUIPC_Write(0x07C8, sizeof(autopilotHeadingLock), &autopilotHeadingLock, &dwResult);
        autopilotNAV1Lock = 1;
        FSUIPC_Write(0x07C4, sizeof(autopilotNAV1Lock), &autopilotNAV1Lock, &dwResult);
        break;
    }

    // process
    FSUIPC_Process(&dwResult);
}

void MainWindow::on_autopilotOffButton_clicked()
{
    short autopilot = 0;
    FSUIPC_Write(0x07BC, sizeof(autopilot), &autopilot, &dwResult);
    FSUIPC_Process(&dwResult);
}

void MainWindow::on_airportButton_clicked()
{
    short com1freq = 8448; //6145;
    FSUIPC_Write(0x034E, sizeof(com1freq), &com1freq, &dwResult);
    short com2freq = 8448;
    FSUIPC_Write(0x3118, sizeof(com2freq), &com2freq, &dwResult);

    short nav1freq = 5136;
    FSUIPC_Write(0x0350, sizeof(nav1freq), &nav1freq, &dwResult);
    short nav2freq = 2448;
    FSUIPC_Write(0x0352, sizeof(nav2freq), &nav2freq, &dwResult);

    long adf = 402719591; // PG: 367.0
    FSUIPC_Write(0x034C, sizeof(adf), &adf, &dwResult);

    FSUIPC_Process(&dwResult);
}
