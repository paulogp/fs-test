#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <Windows.h>
#include "FSUIPC_User.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void readFSUIPCFlight();
    void readFSUIPCAircraft();

private slots:
    void on_startButton_clicked();
    void on_stopButton_clicked();
    void on_lightsButton_clicked();
    void on_setAutopilotButton_clicked();
    void on_autopilotOffButton_clicked();

    void on_airportButton_clicked();

private:
    Ui::MainWindow *ui;
    QTimer * timerFlight;
    QTimer * timerAircraft;

    void startFSUIPC();
    void stopFSUIPC();
    void throttleControl(int level);
    void uiDisplay();
    void aboutDisplay();
    void logDisplay(QString message);
    void control();

    QString radioBCD2Str(short dec);
    QString BCD2Str(short dec);
};

#endif // MAINWINDOW_H
