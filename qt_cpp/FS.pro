#-------------------------------------------------
#
# Project created by QtCreator 2014-06-10T19:42:18
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = FS
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h \
    FSUIPC_User.h

FORMS    += mainwindow.ui

OTHER_FILES += \
    "D:/Users/psportela/Program Files/Microsoft Flight Simulator 2004/Modules/FSUIPC.DLL" \
    "D:/QtProjects/FS/FSUIPCClient.dll"

#QMAKE_LFLAGS_DEBUG += /NODEFAULTLIB:LIBC.lib
#QMAKE_LFLAGS_RELEASE += /NODEFAULTLIB:LIBC.lib

INCLUDEPATH += D:/QtProjects/FS
#LIBS += -D:/Users/psportela/Desktop/FS/FSUIPC_User.lib

#LIBS += -L"D:/QtProjects/FS/" -lFSUIPC_User
win32:LIBS += "D:/QtProjects/FS/FSUIPC_User.lib"
